package files

import (
	"io/ioutil"
	"os"

	"github.com/pkg/errors"
)

// FolderToFiles turns a foldername into a slice of FileInfo.
func FolderToFiles(folder string) (files []os.FileInfo, err error) {
	files, err = ioutil.ReadDir(folder)
	if err != nil {
		err = errors.WithMessagef(err, "could not parse folder %v", folder)
	}
	return
}
