package files

import (
	"os"

	"github.com/pkg/errors"
)

// CreateDirectory creates a directory if it does not exist
func CreateDirectory(dir string) (err error) {
	if _, e := os.Stat(dir); os.IsNotExist(e) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return errors.WithMessagef(err, "could not create directory %v", dir)
		}
	}
	return // if it already exists, no error is needed.
}
