package images

import (
	"image"
	"image/draw"
)

// StackVertically stacks images on top of each other
// and returns the stacked version using the RGBA color model.
// The width of the image is that of the widest image
// and the height is the sum of all the images' heights.
// All images are arranged such that they are horizontally centered
// and vertically on top of each other with no space between.
// If some images are narrower than others, the gap will be filled with transparency.
func StackVertically(first image.Image, others ...image.Image) image.Image {
	var width, height = ReadDimensions(first)
	for _, each := range others {
		otherWidth, otherHeight := ReadDimensions(each)
		if otherWidth > width {
			width = otherWidth
		}
		height += otherHeight
	}

	// draw first image
	result := image.NewRGBA(image.Rect(0, 0, int(width), int(height)))
	rect := first.Bounds()
	rect = rect.Add(image.Pt((int(width)-rect.Dx())/2, 0))
	draw.Draw(result, rect, first, image.Point{}, 0)
	vertical := rect.Dy()

	// now draw others recursively
	for _, img := range others {
		rect = img.Bounds()
		rect = rect.Add(image.Pt((int(width)-rect.Dx())/2, vertical))
		draw.Draw(result, rect, img, image.Point{}, 0)
		vertical += rect.Dy()
	}

	return result
}
