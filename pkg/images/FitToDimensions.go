package images

import (
	"image"
	"image/draw"

	"github.com/nfnt/resize"
)

// FitToDimensions will create an image containing an original
// inside a canvas.X by canvas.Y image that uses the RGBA color model.
// The image will be resized if it exceeds bound.X x bound.Y,
// Having negative coordinates in bound or canvas may lead to unexpected behavior.
func FitToDimensions(img image.Image, bound, canvas image.Point) image.Image {
	result := image.NewRGBA(image.Rectangle{image.Point{}, canvas})

	width, height := ReadDimensions(img)
	if width > uint(bound.X) {
		img = resize.Resize(uint(bound.X), 0, img, resize.Bilinear)
		width, height = ReadDimensions(img)
	}
	if height > uint(bound.Y) {
		img = resize.Resize(0, uint(bound.Y), img, resize.Bilinear)
		width, height = ReadDimensions(img) // dimensions have changed!
	}

	// now let's do some math
	rect := image.Rect(
		(canvas.X-int(width))/2, (canvas.Y-int(height))/2,
		(canvas.X+int(width))/2, (canvas.Y+int(height))/2,
	)
	draw.Draw(result, rect, img, image.Point{}, 0)

	return result
}
