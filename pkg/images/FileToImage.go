package images

import (
	_ "golang.org/x/image/bmp" // BMP support
	_ "image/gif"              // GIF support
	_ "image/jpeg"             // JPG support
	_ "image/png"              // PNG support

	"github.com/pkg/errors"
	"image"
	"os"
)

// FileToImage turns a os.File object into an image.
// This function should support opening of BMP, GIF, JPG, and PNG files.
func FileToImage(file *os.File) (img image.Image, err error) {
	img, _, err = image.Decode(file)
	if err != nil {
		err = errors.WithMessage(err, "cannot open File as an Image")
	}
	return
}
