package images

import "image"

// ReadDimensions reads the dimensions of an image.Image file
// and returns the width and height
func ReadDimensions(img image.Image) (width, height uint) {
	return uint(img.Bounds().Dx()), uint(img.Bounds().Dy())
}
