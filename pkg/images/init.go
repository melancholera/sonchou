// Package images implements functions related to image manipulation.
package images

import (
	_ "golang.org/x/image/bmp" // BMP support
	_ "image/gif"              // GIF support
	_ "image/jpeg"             // JPG support
	_ "image/png"              // PNG support
)
