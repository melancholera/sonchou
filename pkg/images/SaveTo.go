package images

import (
	"image"
	"image/jpeg"
	"image/png"
	"os"

	"github.com/pkg/errors"
)

// SaveToPNG encodes an image to the PNG format and saves it to a file
// If filename already exists it will overwrite it.
func SaveToPNG(img image.Image, filename string) (err error) {
	f, err := os.Create(filename)
	if err != nil {
		return errors.Wrapf(err, "could not save PNG file %v", filename)
	}
	png.Encode(f, img)
	return f.Close()
}

// SaveToJPG encodes an image to the JPG format and saves it to a file
// If filename already exists it will overwrite it.
func SaveToJPG(img image.Image, filename string) (err error) {
	f, err := os.Create(filename)
	if err != nil {
		return errors.Wrapf(err, "could not save JPG file %v", filename)
	}
	jpeg.Encode(f, img, &jpeg.Options{Quality: 100})
	return f.Close()
}
