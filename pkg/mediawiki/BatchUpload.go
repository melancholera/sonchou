package mediawiki

import (
	"path/filepath"

	"github.com/golang/glog"
	"github.com/pkg/errors"
	"gitlab.com/melancholera/go-mwclient"
	"gitlab.com/melancholera/go-mwclient/params"
	"gitlab.com/melancholera/sonchou/pkg/files"
)

// BatchUpload uploads all files in a folder
// which uploads them in their filename
// and includes a function that takes in a filename and returns a map of parameters.
// If paramFunc is nil, they will be uploaded with default options,
// that is, with the same filename as the local file and with no extra parameters.
//
// All folders in folder will be ignored.
// If an error has been encountered for some file,
// it will print the error and stack trace out to glog.Warning
// but will continue uploading the other files.
//
// Remember that paramFunc can overwrite filenames by returning a params.Values
// that has the "filename" parameter
// and can actually skip upload of some files by setting "filename" to "".
// If its "filename" value is set to "", it will ignore the file.
// In addition, if paramFunc returns nil, it will also ignore the file.
//
// Warnings and glog
//
// This script uses the Google logging module https://github.com/golang/glog for fine tuned logging.
// As the function iterates through the folder it may encounter some problems.
//
// It prints to Info if a file is actually a folder,
// or if a file has been successfully uploaded.
//
// It prints to Warning if there is an error in uploading some file.
// Note that it does not return an error immediately.
func BatchUpload(folder string, client *mwclient.Client, paramFunc func(string) params.Values) error {
	if paramFunc == nil {
		paramFunc = func(filename string) params.Values { return nil }
	}

	// read folder
	allFiles, err := files.FolderToFiles(folder)
	if err != nil {
		return errors.Wrapf(err, "could not read folder %v", folder)
	}

	// initialize some variables to be shown later
	total, uploaded := len(allFiles), 0

	for _, eachFile := range allFiles {
		filename := eachFile.Name()
		fullPath := filepath.Join(folder, eachFile.Name())

		if eachFile.IsDir() {
			glog.Infof("%v is a folder, skipping", filename)
			continue // don't care
		}

		// open file
		parameters := paramFunc(filename)
		if parameters == nil {
			continue
		}
		if v, ok := parameters["filename"]; ok && v == "" {
			continue // don't care.
		}
		response, err := OpenAndUpload(fullPath, filename, client, parameters)
		if err != nil {
			glog.Warningf("Error in uploading %v: %v", filename, err)
			glog.Warningf("Stack trace: \n%+v\n", err)
		} else {
			glog.Infof("Uploaded %v: %v", filename, response)
			uploaded++
		}
	}

	glog.Infof("Uploaded %v files out of %v", uploaded, total)

	return nil
}
