// Package mediawiki contains custom queries
// which tap into the MediaWiki API using go-mwclient
// (gitlab.com/melancholera/go-mwclient).
//
// Bot passwords
//
// The user could use a bot password to log into their mwclient.Client instance.
// See https://www.mediawiki.org/wiki/Manual:Bot_passwords for more information.
//
// Because most functions in this package will require the user to be logged in,
// users of this package should be careful about hosting their passwords online.
//
// Liability
//
// While the author of this package has tried their best to make this package
// work as intended, responsibility still lies on the user.
// The author is not, and cannot be held, responsible for any damages
// intentional or otherwise caused by users of this package.
//
// Normalization and denormalization
//
// Normalization and denormalization functions are written so that links in a page could be spotted
// even if they are written in a really different way. For instance, `[[Some title]]`,
// `[[Some_title]]`, `[[:Some title]]`, and `[[_ _:Some _ _ title _ ]]` will refer to the same page
// titled "Some title", but `[[Some Title]]` and `[[Some title with text]]` won't.
// The denormalization strategy is somewhat naive: for instance, it does not detect URL encoded links.
//
// Refer to https://www.mediawiki.org/wiki/Manual:Title.php#Title_structure for more information.
package mediawiki

import _ "gitlab.com/melancholera/go-mwclient" // to be used for later

// Some constants that can be recycled
const (
	NookipediaAPIUrl = "https://nookipedia.com/w/api.php"
	DefaultUserAgent = "[https://gitlab.com/melancholera/sonchou/ sonchou]"
)
