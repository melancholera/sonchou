package mediawiki

import (
	"github.com/golang/glog"
	"github.com/pkg/errors"
	"gitlab.com/melancholera/go-mwclient"
	"gitlab.com/melancholera/go-mwclient/params"
)

// QueryCategoryMembers returns a channel containing the pages, subcategories,
// and files in a category. The channel is closed afterwards.
// Note that cateogry does not need the prefix "Category:".
// If there are any errors in querying, they will be printed to glog.Error,
// but they will not be returned.
func QueryCategoryMembers(category string, client *mwclient.Client) chan string {
	members := make(chan string, 10) // 10 is a good number
	go func() {
		defer close(members)
		query := client.NewQuery(params.Values{
			"list":    "categorymembers",
			"cmtitle": "Category:" + category,
			"cmprop":  "ids|title",
		})

		for query.Next() {
			pages, err := query.Resp().GetObjectArray("query", "categorymembers")
			if err != nil {
				err = errors.Wrap(err, "could not get object array using [query][categorymembers]")
				glog.Errorf("Error in accessing pages: %v", err)
				glog.Errorf("Stack trace:\n%+v\n", err)
				continue
			}
			for _, page := range pages {
				title, err := page.GetString("title")
				if err != nil {
					err = errors.Wrapf(err, "could not get page title using [title]")
					glog.Errorf("Error in accessing page title: %v", err)
					glog.Errorf("Stack trace:\n%+v\n", err)
				}
				members <- title
			}
		}

		if query.Err() != nil {
			glog.Errorf("Error in querying category %v: %v", category, query.Err())
			glog.Errorf("Stack trace: \n%+v\n", query.Err())
		}
	}()

	return members
}

// QueryLinks returns a channel containing the names of all links of a page
// and will close it afterwards.
// If there are any errors in querying, they will be printed to glog.Error,
// but they will not be returned.
func QueryLinks(title string, client *mwclient.Client) chan string {
	links := make(chan string, 10) // 10 is a good number
	go func() {
		defer close(links)
		query := client.NewQuery(params.Values{
			"prop":   "links",
			"titles": title,
		})

		for query.Next() {
			// query[query][pages] returns a list of jason Objects.
			// We'll have to access the latter's links parameter to get the titles
			listPages, err := query.Resp().GetObjectArray("query", "pages")
			if err != nil {
				err = errors.Wrap(err, "could not get object array using [query][pages]")
				glog.Errorf("Error in accessing pages: %v", err)
				glog.Errorf("Stack trace:\n%+v\n", err)
				continue
			}

			for _, pages := range listPages {
				// pages[title] : title parameter of PerformInLinks
				// pages[links] : a list of objects which we'd want to take a look at
				listLinks, err := pages.GetObjectArray("links")
				if err != nil {
					err = errors.Wrapf(err, "could not get object array using [links]: %#v", pages)
					glog.Errorf("Error in accessing list of links: %v", err)
					glog.Errorf("Stack trace:\n%+v\n", err)
				}

				for _, link := range listLinks {
					// link[title]: the title of the link!
					linkTitle, err := link.GetString("title")
					if err != nil {
						err = errors.Wrapf(err, "could not get link title using [title]: %#v", link)
						glog.Errorf("Error in accessing link: %v", err)
						glog.Errorf("Stack trace:\n%+v\n", err)
					}
					links <- linkTitle
				}
			}
		}

		if query.Err() != nil {
			glog.Errorf("Error in querying %v: %v", title, query.Err())
			glog.Errorf("Stack trace: \n%+v\n", query.Err())
		}
	}()
	return links
}

// QueryFiles returns a channel containing the names of the files used in a page
// and will close it afterwards.
// Note that the output strings will start with "File:".
//
// If there are any errors in querying, they will be printed to glog.Error,
// but they will not be returned.
func QueryFiles(title string, client *mwclient.Client) chan string {
	files := make(chan string, 10) // 10 is a good number
	go func() {
		defer close(files)
		query := client.NewQuery(params.Values{
			"prop":   "images", // https://www.mediawiki.org/w/api.php?action=help&modules=query%2Bimages
			"titles": title,
		})

		for query.Next() {
			// query[query][pages] returns a list of jason Objects.
			// We'll have to access the latter's links parameter to get the titles
			listPages, err := query.Resp().GetObjectArray("query", "pages")
			if err != nil {
				err = errors.Wrap(err, "could not get object array using [query][pages]")
				glog.Errorf("Error in accessing pages: %v", err)
				glog.Errorf("Stack trace:\n%+v\n", err)
				continue
			}

			for _, pages := range listPages {
				// pages[title] : title parameter of PerformInFiles
				// pages[images] : a list of objects which we'd want to take a look at
				listFiles, err := pages.GetObjectArray("images")
				if err != nil {
					err = errors.Wrapf(err, "could not get object array using [images]: %#v", pages)
					glog.Errorf("Error in accessing list of files: %v", err)
					glog.Errorf("Stack trace:\n%+v\n", err)
				}

				for _, file := range listFiles {
					// link[title]: the title of the link!
					var fileTitle string
					if fileTitle, err = file.GetString("title"); err != nil {
						err = errors.Wrapf(err, "could not get link title using [title]: %#v", file)
						glog.Errorf("Error in accessing link: %v", err)
						glog.Errorf("Stack trace:\n%+v\n", err)
					}

					files <- fileTitle
				}
			}
		}

		if query.Err() != nil {
			glog.Errorf("Error in querying %v: %v", title, query.Err())
			glog.Errorf("Stack trace: \n%+v\n", query.Err())
		}
	}()
	return files
}

// QueryUnusedImages returns a channel containing all the names of the files
// that have been unused (Special:UnusedFiles)
// and will close it afterwards.
// Note that the output strings will start with "File:".
//
// If there are any errors in querying, they will be printed to glog.Error,
// but they will not be returned.
func QueryUnusedImages(client *mwclient.Client) chan string {
	members := make(chan string, 10) // 10 is a good number
	go func() {
		defer close(members)
		query := client.NewQuery(params.Values{
			"action": "query",
			"list":   "querypage",
			"qppage": "Unusedimages",
		})

		for query.Next() {
			pages, err := query.Resp().GetObjectArray("query", "querypage", "results")
			if err != nil {
				err = errors.Wrap(err, "could not get object array using [query][querypage][results]")
				glog.Errorf("Error in accessing pages Unusedimages: %v", err)
				glog.Errorf("Stack trace:\n%+v\n", err)
				continue
			}
			for _, page := range pages {
				title, err := page.GetString("title")
				if err != nil {
					err = errors.Wrapf(err, "could not get page title using [title]")
					glog.Errorf("Error in accessing page title: %v", err)
					glog.Errorf("Stack trace:\n%+v\n", err)
				}
				members <- title
			}
		}

		if query.Err() != nil {
			glog.Errorf("Error in querying unused images: %v", query.Err())
			glog.Errorf("Stack trace: \n%+v\n", query.Err())
		}

	}()
	return members
}

// QueryUncategorizedImages returns a channel containing all the names of the files
// that have been uncategorized (Special:UncategorizedFiles)
// and will close it afterwards.
// Note that the output strings will start with "File:".
//
// If there are any errors in querying, they will be printed to glog.Error,
// but they will not be returned.
func QueryUncategorizedImages(client *mwclient.Client) chan string {
	members := make(chan string, 10) // 10 is a good number
	go func() {
		defer close(members)
		query := client.NewQuery(params.Values{
			"list":   "querypage",
			"qppage": "Uncategorizedimages",
		})

		for query.Next() {
			pages, err := query.Resp().GetObjectArray("query", "querypage", "results")
			if err != nil {
				err = errors.Wrap(err, "could not get object array using [query][querypage][results]")
				glog.Errorf("Error in accessing pages Uncategorizedimages: %v", err)
				glog.Errorf("Stack trace:\n%+v\n", err)
				continue
			}
			for _, page := range pages {
				title, err := page.GetString("title")
				if err != nil {
					err = errors.Wrapf(err, "could not get page title using [title]")
					glog.Errorf("Error in accessing page title: %v", err)
					glog.Errorf("Stack trace:\n%+v\n", err)
				}
				members <- title
			}
		}

		if query.Err() != nil {
			glog.Errorf("Error in querying unused images: %v", query.Err())
			glog.Errorf("Stack trace: \n%+v\n", query.Err())
		}

	}()
	return members
}

// QuerySpecialPage returns a channel containing pages in some special page qppage.
// The channel will close once all pages have been printed.
//
// Note that it is case sensitive (e.g., "Ancientpages", "Lonelypages").
// See https://www.mediawiki.org/wiki/API:Querypage for possible values of qppage.
//
// If there are any errors in querying, they will be printed to glog.Error,
// but they will not be returned.
func QuerySpecialPage(client *mwclient.Client, qppage string) chan string {
	members := make(chan string, 16) // 16 is a good number
	go func() {
		defer close(members)
		query := client.NewQuery(params.Values{
			"list":   "querypage",
			"qppage": qppage,
		})

		for query.Next() {
			pages, err := query.Resp().GetObjectArray("query", "querypage", "results")
			if err != nil {
				err = errors.Wrap(err, "could not get object array using [query][querypage][results]")
				glog.Errorf("Error in accessing pages %v: %v", qppage, err)
				glog.Errorf("Stack trace:\n%+v\n", err)
				continue
			}
			for _, page := range pages {
				title, err := page.GetString("title")
				if err != nil {
					err = errors.Wrapf(err, "could not get page title using [title]")
					glog.Errorf("Error in accessing page title: %v", err)
					glog.Errorf("Stack trace:\n%+v\n", err)
				}
				members <- title
			}
		}

		if query.Err() != nil {
			glog.Errorf("Error in querying %v: %v", qppage, query.Err())
			glog.Errorf("Stack trace: \n%+v\n", query.Err())
		}
	}()
	return members
}
