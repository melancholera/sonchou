package mediawiki

import (
	"regexp"
	"strings"
	"unicode"

	"github.com/pkg/errors"
	"gitlab.com/melancholera/go-mwclient"
	"gitlab.com/melancholera/go-mwclient/params"
)

// blank spaces
var (
	WikiTextBlank       = regexp.MustCompile(`[\t _\xA0\x{1680}\x{180E}\x{2000}-\x{200A}\x{2028}\x{2029}\x{202F}\x{205F}\x{3000}]+`)
	WikiTextBlankOrBidi = regexp.MustCompile(`[\t _\xA0\x{1680}\x{180E}\x{2000}-\x{200B}\x{200E}\x{200F}\x{2028}-\x{202F}\x{205F}\x{3000}]*`)
)

// WikiSpecial denotes special characters (in RegEx) that can be found in Mediawiki titles
const WikiSpecial = `\^$.|?*+()[]{}`

// denormalizeLetter returns a regex string that encodes the lowercase, uppercase, and titlecase runes.
// For example, d becomes [d|D], and ǳ becomes [Ǳ|ǲ|Ǳ].
func denormalizeLetter(letter rune) string {
	if strings.ContainsRune(WikiSpecial, letter) {
		return "\\" + string(letter)
	}
	lower := unicode.ToLower(letter)
	upper := unicode.ToUpper(letter)
	if lower == upper {
		return string(letter) // no diference
	}
	var sb strings.Builder
	sb.WriteRune('[')
	sb.WriteRune(lower)
	sb.WriteRune('|')
	sb.WriteRune(upper)
	if title := unicode.ToTitle(letter); upper != title {
		sb.WriteRune('|')
		sb.WriteRune(title)
	}
	sb.WriteRune(']')
	return sb.String()
}

// NormalizeTitle normalizes a title by setting its first character to uppercase and all whitespace to a space.
// For example, NormalizeTitle("New_Year's  Day") turns into "New Year's Day".
func NormalizeTitle(title string) string {
	if len(title) == 0 {
		return ""
	}
	title = WikiTextBlank.ReplaceAllString(title, " ")
	title = strings.TrimSpace(title)
	inputRaw := []rune(title)
	inputRaw[0] = unicode.ToTitle(inputRaw[0])
	title = string(inputRaw)
	return title
}

// DenormalizeTitle denormalizes a title and returns a string that encodes a regex string.
// Its first match is the title.
func DenormalizeTitle(title string) string {
	if len(title) == 0 {
		return "" // do nothing
	}
	title = NormalizeTitle(title) // normalize it first!

	var sb strings.Builder
	rawTitle := []rune(title)

	sb.WriteString(WikiTextBlankOrBidi.String()) // can start with a space
	sb.WriteRune('(')                            // start grp

	sb.WriteString(denormalizeLetter(rawTitle[0]))
	for ii := 1; ii < len(rawTitle); ii++ {
		if rawTitle[ii] == ' ' {
			sb.WriteString(WikiTextBlank.String())
		} else if strings.ContainsRune(WikiSpecial, rawTitle[ii]) {
			sb.WriteRune('\\')
			sb.WriteRune(rawTitle[ii])
		} else {
			sb.WriteRune(rawTitle[ii])
		}
	}

	sb.WriteRune(')')                            // end grp
	sb.WriteString(WikiTextBlankOrBidi.String()) // can end with a space

	return sb.String()
}

// NormalizeNamespace turns a namespace into its normalized form.
// It sets all characters to lowercase and the first character to uppercase.
func NormalizeNamespace(namespace string) string {
	if len(namespace) == 0 {
		return ""
	}
	namespace = WikiTextBlank.ReplaceAllString(namespace, " ")
	namespace = strings.TrimSpace(namespace)
	inputRaw := []rune(namespace)
	inputRaw[0] = unicode.ToTitle(inputRaw[0])
	for ii := 1; ii < len(inputRaw); ii++ {
		inputRaw[ii] = unicode.ToLower(inputRaw[ii])
	}
	return string(inputRaw)
}

// DenormalizeNamespace creates a regex string that matches either the localized or canonical namespace.
// If the two are equal, or if localized is blank, it will denormalize the canonical namespace.
// Its first match is the namespace.
func DenormalizeNamespace(localized, canonical string) string {
	localized, canonical = NormalizeNamespace(localized), NormalizeNamespace(canonical)

	var sb strings.Builder
	sb.WriteString(WikiTextBlankOrBidi.String())
	sb.WriteRune('(') // start grp

	if localized != canonical && localized != "" { // localized exists
		for _, vv := range localized {
			if vv == ' ' {
				sb.WriteString(WikiTextBlank.String())
			} else {
				sb.WriteString(denormalizeLetter(vv))
			}
		}
		sb.WriteRune('|') // OR group
	}

	for _, vv := range canonical {
		if vv == ' ' {
			sb.WriteString(WikiTextBlank.String())
		} else {
			sb.WriteString(denormalizeLetter(vv))
		}
	}

	sb.WriteRune(')') // end grp
	sb.WriteString(WikiTextBlankOrBidi.String())
	return sb.String()
}

// NormalizeLink creates the normalized version of a link provided an interwiki,
// localized and canonical namespaces, and title.
// It will not add a colon after "[[" e.g., "[[Category:Something]]" instead
// of "[[:Category:Something]]"
func NormalizeLink(interwiki, localized, canonical, title string) string {
	var sb strings.Builder
	sb.WriteString("[[")

	if interwiki != "" {
		interwiki = NormalizeNamespace(interwiki)
		interwiki = strings.ToLower(interwiki)
		sb.WriteString(interwiki)
		sb.WriteRune(':')
	}

	if canonical != "" {
		canonical = NormalizeNamespace(canonical)
		if localized != "" {
			localized = NormalizeNamespace(localized)
			sb.WriteString(localized)
		} else {
			sb.WriteString(canonical)
		}
		sb.WriteRune(':')
	}

	sb.WriteString(NormalizeTitle(title))
	sb.WriteString("]]")
	return sb.String()
}

// DenormalizeLink creates a regex string that matches links that refer to a specific page
// with some specific interwiki prefix, localized and canonical namespaces, and title.
// For example, localized:Fichier, canonical:File, title:Example.png will find
// [[  :  File:   Example.png   ]], [[Fichier:_Example.png]], and [[File:Example.png|some text]].
// Its first match for the above would be "  :  File:   Example.png   ", "Fichier:_Example.png", and "File:Example.png".
//
// Note that canonical must be non-empty for categories to be detected.
// localized may be empty.
func DenormalizeLink(interwiki, localized, canonical, title string) string {
	var sb strings.Builder
	sb.WriteString(`\[\[`)                       // initialize link
	sb.WriteString(WikiTextBlankOrBidi.String()) // any amount of space...
	sb.WriteRune('(')                            // start of group

	sb.WriteString(":?") // there could be a colon before everything...

	// check interwiki. Note that interwikis are just namespaces.
	if interwiki != "" {
		sb.WriteString(DenormalizeNamespace(interwiki, interwiki))
		sb.WriteRune(':')
	}

	// check categories
	if canonical != "" {
		sb.WriteString(DenormalizeNamespace(localized, canonical))
		sb.WriteRune(':')
	}

	// add page. It's guaranteed that title isn't blank.
	sb.WriteString(DenormalizeTitle(title))

	sb.WriteRune(')') // end of group
	// now this is where it's either a pipe ("|"") and some text, or nothing, and then the closing for the link ("]]")
	sb.WriteString(`(?:\|[^\]]*)?`) // capture the piped text if it does exist
	sb.WriteString(`\]\]`)
	return sb.String()
}

// denormalizeCategory is like DenormalizeLink but for a specific category
func denormalizeCategory(localized, category string) string {
	var sb strings.Builder
	sb.WriteString(`\[\[`)
	sb.WriteString(WikiTextBlankOrBidi.String())
	sb.WriteRune('(')
	sb.WriteString(DenormalizeNamespace(localized, "Category"))
	sb.WriteRune(':')
	sb.WriteString(DenormalizeTitle(category))
	sb.WriteRune(')')
	sb.WriteString(`(?:\|[^\]]*)?`) // capture the piped text if it does exist
	sb.WriteString(`\]\]`)
	return sb.String()
}

// RecategorizeContent changes a text such that it will remove all categories found in oldCat
// and will append all categories in newCat.
// It will also return a necessary error in case it couldn't compile any regex expression.
// If localized is blank the category namespace will be the canonical "Category:".
//
// Removal will also try to remove any spaces or newlines before or after
// the declaration of [[Category:Some category]] for categories in oldCat.
// Appending categories from newCat will be done at the very end of the text
// where the categories are in their canonical form.
func RecategorizeContent(content string, oldCat, newCat []string, localized string) (string, error) {
	for _, category := range oldCat {
		var toRemove strings.Builder
		toRemove.WriteString(`[[:space:]]*`)
		toRemove.WriteString(denormalizeCategory(localized, category))
		toRemove.WriteString(`[[:space:]]*`)
		findOld, err := regexp.Compile(toRemove.String())
		if err != nil {
			return "", errors.Wrapf(err, "could not compile regex for %v", category)
		}
		content = findOld.ReplaceAllString(content, "\n")
	}

	var newContent strings.Builder
	newContent.WriteString(content)
	for _, category := range newCat {
		// check first if already in newContent...
		toAdd := NormalizeLink("", localized, "Category", category)
		toRemove := denormalizeCategory(localized, category)
		findNew, err := regexp.Compile(toRemove)
		if err != nil {
			return "", errors.Wrapf(err, "could not compile regex for %v", category)
		}
		if findNew.MatchString(newContent.String()) {
			continue // do nothing since it's already in content
		}
		newContent.WriteRune('\n')
		newContent.WriteString(toAdd)
	}

	return newContent.String(), nil
}

// Recategorize is a PageFunction that takes in two optional parameters:
// a slice of strings containing the categories it wishes to remove (optional[0]),
// and a slice of strings containing the categories it wishes to add (optional[1]).
// If type assertion could not be done, or if optional is too short,
// the function will panic.
//
// Note that it is possible for either optional[0] or optional[1] to be
// empty string slices ([]string{}),
// that is, either remove or add no categories,
// but not nil.
//
// The slices for old and new categories do not need to start with "Category:".
//
// Methodology for categorization
//
// Recategorization uses the normalization-denormalization technique.
//
// Do note that "Category:" will be replaced by the localized namespace that the client uses.
// In addition, the function will first check if "[[Category:NEWCAT]]" is in the
// string before adding one at the very end.
//
// After replacing said text, it will then edit the page with the edit summary
// "Recategorized: -[[:Category:OldCat1]] -[[:Category:OldCat2]] ... +[[:Category:NewCat1]] +[[:Category:NewCat2]] ... using client.UserAgent".
// If there were no changes performed, it will return a DidNothingError.
//
// The edit will be marked as a Bot edit.
//
// Bugs
//
// This function will not check URL encoded strings, for example, [[Category:Some %2F thing]].
func Recategorize(pagename string, client *mwclient.Client, optional ...interface{}) error {
	oldCat, newCat := optional[0].([]string), optional[1].([]string)
	// Now what to do with these...
	localized, err := client.LocalizedNamespace("Category")
	canonical := "Category"
	if err != nil {
		localized = canonical
	}

	oldContent, timestamp, err := client.GetPageByName(pagename)
	if err != nil {
		return errors.Wrapf(err, "could not get page %s", pagename)
	}
	newContent, err := RecategorizeContent(oldContent, oldCat, newCat, localized)
	if err != nil {
		panic(err)
	}
	// now upload
	if newContent == oldContent {
		return DidNothingError{Pagename: pagename}
	}
	err = client.Edit(params.Values{
		"title": pagename,
		"text":  newContent,
		"summary": func() string {
			var sb strings.Builder
			sb.WriteString("Recategorized: ")
			for _, category := range oldCat {
				sb.WriteString(`-`)
				sb.WriteString(NormalizeLink("", localized, canonical, category))
				sb.WriteString(`, `)
			}
			for _, category := range newCat {
				sb.WriteString(`+`)
				sb.WriteString(NormalizeLink("", localized, canonical, category))
				sb.WriteString(`, `)
			}
			sb.WriteString("using ")
			sb.WriteString(client.UserAgent)
			sb.WriteString(".")
			return sb.String()
		}(),
		"basetimestamp": timestamp,
		"bot":           "1",
	})
	if err != nil {
		return errors.Wrapf(err, "could not edit page %s", pagename)
	}
	return nil
}
