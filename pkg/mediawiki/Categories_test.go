package mediawiki

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDenormalizeLetter(t *testing.T) {
	assert := assert.New(t)
	cases := []struct {
		input  string
		lookup rune // rune to look for in input
		count  uint // the number of instances of lookup
	}{
		{
			input:  "There is some consideration that Renée is the best character",
			lookup: 'r',
			count:  5,
		},
		{
			input:  "This string's (kinda arbitrary) value is 2135963254. There are exactly three 3's in this string.",
			lookup: '3',
			count:  3,
		},
		{
			input:  "Lowercase: `ǳ`, titlecase: `ǲ`, uppercase: `Ǳ`",
			lookup: 'Ǳ',
			count:  3,
		},
	}

	for _, tt := range cases {
		regex, err := regexp.Compile(denormalizeLetter(tt.lookup))
		assert.NoError(err, "should not have error", tt)
		all := regex.FindAllString(tt.input, -1)
		assert.EqualValues(tt.count, len(all), "should have equal lengths", tt)
	}
}

func TestNormalizeDenormalizeTitle(t *testing.T) {
	assert := assert.New(t)
	// test out normalization and denormalization of titles
	cases := []struct {
		normalized string
		tests      []struct {
			title string // non-normalized
			valid bool   // is Normalize(title) equals to normalized?
		}
	}{
		{"", []struct {
			title string
			valid bool
		}{
			{"", true},
		}},
		{"New Year's Day", []struct {
			title string
			valid bool
		}{
			{"New Year's Day", true},
			{"    \tNew Year's_Day", true},
			{"New year's day", false},
			{"__new Year's _  __  Day____", true},
			{"New Year\"s Day", false},
		}},
		{"Bugs/City Folk", []struct {
			title string
			valid bool
		}{
			{"Bugs/City Folk", true},
			{" __ Bugs/City_Folk   ", true},
			{"Bugs/City\u180EFolk", true},
			{"bugs/City_Folk   ", true},
			{"Bugs/city Folk", false},
			{"Bugs\\City Folk", false},
			{"Bugs City Folk", false},
			{"Bugs/City Folk and New Leaf", false},
		}},
		{"June (villager)", []struct {
			title string
			valid bool
		}{
			{`  June_(villager) _`, true},
			{`  June_\(villager\)`, false},
		}},
		{"#See also", []struct {
			title string
			valid bool
		}{
			{"__#See_also__", true},
			{"__#see also", false},
		}},
	}

	for _, tt := range cases {
		// note that ^ and & are there to ensure start and end of string
		denormalized, err := regexp.Compile("^" + DenormalizeTitle(tt.normalized) + "$")
		assert.NoErrorf(err, "compiling %v should not error", tt.normalized)
		for _, vv := range tt.tests {
			found := denormalized.MatchString(vv.title)
			assert.Equalf(vv.valid, found, "check if %v catches %v", denormalized.String(), vv.title)
			assert.Equalf(vv.valid, tt.normalized == NormalizeTitle(vv.title), "check if %v normalizes to %v", vv.title, tt.normalized)
		}
	}
}

func TestNormalizeDenormalizeCategories(t *testing.T) {
	assert := assert.New(t)
	cases := []struct {
		localized string
		canonical string
		tests     []struct {
			namespace string
			valid     bool
		}
	}{
		{"", "", []struct {
			namespace string
			valid     bool
		}{
			{"", true},
			{" ", true},
			{"Not empty", false},
		}},
		{"", "File", []struct {
			namespace string
			valid     bool
		}{
			{"", false},
			{"File", true},
			{"__fILe__", true},
			{"File talk", false},
			{"Media", false}, // well...
		}},
		{"Discussion fichier", "File talk", []struct {
			namespace string
			valid     bool
		}{
			{"  _ Discussion_Fichier  ", true},
			{"File_Talk", true},
			{"File Fichier  ", false},
			{"Talk", false},
			{"___FiLe_TalK  ", true},
		}},
	}

	for _, tt := range cases {
		denormalized, err := regexp.Compile("^" + DenormalizeNamespace(tt.localized, tt.canonical) + "$")
		assert.NoErrorf(err, "compiling %v and %v should not error", tt.localized, tt.canonical)
		for _, vv := range tt.tests {
			found := denormalized.MatchString(vv.namespace)
			assert.Equalf(vv.valid, found, "check if %v catches %v", denormalized.String(), vv.namespace)
		}
	}
}

func TestDenormalizeLink(t *testing.T) {
	assert := assert.New(t)
	cases := []struct {
		interwiki string
		localized string
		canonical string
		title     string
		tests     []struct {
			link  string
			valid bool
		}
	}{
		{``, ``, ``, `Animal Crossing`, []struct {
			link  string
			valid bool
		}{
			{`[[Animal Crossing]]`, true},
			{`[[animal_Crossing]]`, true},
			{`[[Animal crossing]]`, false},
			{`[[Animal Crossing]] `, false}, // exact match needed!
			{`[[Animal Crossing: new leaf]]`, false},
			{`[[animal Crossing`, false},
			{`[[animal_Crossing|]]`, true},
			{`[[:Animal Crossing|some text]]`, true},
			{`[[: _ _ _ Animal  _   Crossing _ _ ]]`, true},
		}},
		{``, ``, ``, `bugs/City Folk`, []struct {
			link  string
			valid bool
		}{
			{`[[Bugs/City Folk]]`, true},
			{`[[:bugs/City_Folk]]`, true},
			{`[[Bugs/City Folk|some text]]`, true},
			{`[[Bugs\City Folk]]`, false},
			{`[[Bugs/city Folk]]`, false},
			{`[[    :  Bugs/City __ __ Folk______]]`, true},
			{`[[Bugs/CIty Folk]]`, false},
			{`[[Bugs/City Folk:]]`, false},
		}},
		{``, ``, `category talk`, `some page`, []struct {
			link  string
			valid bool
		}{
			{`[[Category talk:Some page]]`, true},
			{`[[Category Talk:Some page]]`, true},
			{`[[Category talk:Some Page]]`, false},
			{`[[___ _ Category___talk__:_  _ __Some_ _ page   ]]`, true},
			{`[[ _ :__ Category TaLk:Some page]]`, true},
			{`[[Category:Some page]]`, false},
			{`[[Category talk:Some page more text]]`, false},
			{`Text [[Category talk:Some page]]`, false},
			{`[[Category talk:Some page|some text]]`, true},
		}},
		{``, `discussion catégorie`, `CATEGORY_TALK`, `langue allemande   `, []struct {
			link  string
			valid bool
		}{
			{`[[Discussion catégorie:Langue allemande]]`, true},
			{`[[Category talk:Langue allemande]]`, true},
			{`[[Category talk:Germanic languages]]`, false},
			{`[[__Category talk:Langue allemande`, false},
			{`[[:Discussion catégorie:Langue allemande|  sil vous plait  ]]`, true},
			{`[[::Discussion catégorie:Langue allemande]]`, false},
			{`[[:Langue allemande]]`, false},
			{`[[Discussion catégorie:Category talk:Langue allemande]]`, false},
			{`[[Discussion catègorie:Langue allemande]]`, false},
		}},
		{`FR`, `Discussion catégorie`, `Category talk`, `Langue allemande`, []struct {
			link  string
			valid bool
		}{
			{`[[fr:Discussion catégorie:Langue allemande]]`, true},
			{`[[Discussion catégorie: Langue allemande]]`, false},
			{`[[:fr:Category talk:Langue allemande   ]]`, true},
			{`[[:fR:CAtEgORy___ _taLk :  langue allemande  ]]`, true},
		}},
	}

	for _, tt := range cases {
		denormalizedRaw := DenormalizeLink(tt.interwiki, tt.localized, tt.canonical, tt.title)
		denormalized, err := regexp.Compile("^" + denormalizedRaw + "$")
		assert.NoErrorf(err, "compiling %v should not error", denormalizedRaw)
		for _, vv := range tt.tests {
			found := denormalized.MatchString(vv.link)
			assert.Equalf(vv.valid, found, "check if %v catches %v", denormalized.String(), vv.link)
		}
	}
}

func TestNormalizeLink(t *testing.T) {
	assert := assert.New(t)
	cases := []struct {
		interwiki, localized, canonical, title string
		want                                   string
	}{
		{``, ``, ``, `Some page`, `[[Some page]]`},
		{``, ``, ``, `_some other_ _ Page__`, `[[Some other Page]]`},
		{``, ``, `cateGory_talk`, `Some category  `, `[[Category talk:Some category]]`},
		{``, ``, ``, `Category tAlk:Some category`, `[[Category tAlk:Some category]]`}, // won't normalize namespace!
		{``, `discussion_catégorIe`, `CategoRy talK`, `Langue allemande`, `[[Discussion catégorie:Langue allemande]]`},
		{`mw`, ``, `Help`, `Images`, `[[mw:Help:Images]]`},
		{`mw`, ``, ``, `Help:Images`, `[[mw:Help:Images]]`},
		{`FR`, `Discussion Catégorie`, `Category Talk`, `Langue allemande`, `[[fr:Discussion catégorie:Langue allemande]]`},
	}
	for _, tt := range cases {
		normalized := NormalizeLink(tt.interwiki, tt.localized, tt.canonical, tt.title)
		assert.Equal(tt.want, normalized, tt)
	}
}
