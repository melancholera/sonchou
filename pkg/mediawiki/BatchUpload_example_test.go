package mediawiki_test

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/melancholera/go-mwclient"
	"gitlab.com/melancholera/go-mwclient/params"
	"gitlab.com/melancholera/sonchou/pkg/mediawiki"
)

// The following example is what I have used in
// https://nookipedia.com/wiki/Category:Animal_Crossing:_Wild_World_furniture_items.
// Do not forget to handle errors.
func ExampleBatchUpload() {
	folder := "folder to open"
	client, err := mwclient.New(mediawiki.NookipediaAPIUrl, mediawiki.DefaultUserAgent)
	if err != nil {
		err = errors.Wrap(err, "could not instantiate mwclient object")
		return
	}

	err = client.Login("username", "password")
	if err != nil {
		err = errors.Wrap(err, "could not log in")
		return
	}

	err = mediawiki.BatchUpload(folder, client, func(filename string) params.Values {
		// make sure filename fits "...WW Icon.png"
		values := make(params.Values)
		if !strings.HasSuffix(filename, "WW.png") {
			values["filename"] = "" // skip!
		}

		values["comment"] = fmt.Sprintf("Uploaded via %v", mediawiki.DefaultUserAgent)

		// generate the upload text
		text := new(strings.Builder)
		text.WriteString("Image of the ")
		text.WriteString(strings.TrimSuffix(filename, "WW.png"))
		text.WriteString(" from {{WW}}. Taken from [https://animalcrossingwiki.de/acww/katalog/einrichtung Einrichtungsgegenstände] from [https://animalcrossingwiki.de/ Animal Crossing Wiki].\n\n")
		text.WriteString("== Licensing ==\n{{game_sprite}}\n\n[[Category:Animal Crossing: Wild World furniture items]]")
		values["text"] = text.String()

		return values
	})

	return
}
