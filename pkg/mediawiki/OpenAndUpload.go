package mediawiki

import (
	"os"

	"github.com/antonholmquist/jason"
	"github.com/pkg/errors"
	"gitlab.com/melancholera/go-mwclient"
	"gitlab.com/melancholera/go-mwclient/params"
)

// OpenAndUpload opens a local file and uploads it with a filename remote with some parameters.
// Note that remote does not need to start with "File:".
// Returns the response and an error.
func OpenAndUpload(local, remote string, client *mwclient.Client, params params.Values) (*jason.Object, error) {
	file, err := os.Open(local)
	if err != nil {
		return nil, errors.Wrapf(err, "could not open file %v", local)
	}

	response, err := client.Upload(file, remote, params)
	if err != nil {
		return nil, errors.Wrapf(err, "could not upload file %v to %v", local, remote)
	}

	return response, nil
}
