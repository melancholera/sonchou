package mediawiki

import (
	"fmt"
	"strings"

	"github.com/golang/glog"
	"github.com/pkg/errors"
	"gitlab.com/melancholera/go-mwclient"
	"gitlab.com/melancholera/go-mwclient/params"
)

// DidNothingError is an "error" type that a PageFunction can return if it encounters a page
// and it explicitly chooses to do nothing.
type DidNothingError struct {
	Pagename string
}

func (e DidNothingError) Error() string {
	return fmt.Sprintf("did nothing for %v", e.Pagename)
}

// PageFunction is a function that takes in a MediaWiki page's name, a *mwclient.Client, and optional parameters,
// and returns an optional return value and an error.
//
// Usage
//
// PageFunction may take in arbitrary values, more specifically, empty interfaces.
// Due to the unsafe nature of empty interfaces,
// care must be taken in specifying the types of these optional parameters
// or risk a panic.
//
// Namespaces
//
// PageFunction does not necessarily know what namespace pagename belongs to,
// although it can be inferred manually using its prefix (e.g., "File:").
//
// See the variables for sample PageFunctions.
type PageFunction func(pagename string, client *mwclient.Client, optional ...interface{}) error

// CategorizeToNLVillagerHouses is an example of a PageFunction
// used to categorize images to https://nookipedia.com/wiki/Category:New_Leaf_villager_houses.
// It ignores any optional parameters.
//
// This function takes in a name of a pagename and categorizes it
// by naïvely replacing "[[Category:New leaf images]]" with "[[Category:New Leaf villager houses]]".
// If pagename does not start with "File:House of " or does not end with either " NL.jpg", " NL.jpeg", or " NL.png";
// or if its contents already contain "[[Category:New Leaf villager houses]]",
// it will return a DidNothingError.
// Otherwise it will return whatever error occurs after editing.
var CategorizeToNLVillagerHouses PageFunction = func(pagename string, client *mwclient.Client, optional ...interface{}) error {
	if !strings.HasPrefix(pagename, "File:House of ") ||
		!(strings.HasSuffix(pagename, " NL.jpg") ||
			strings.HasSuffix(pagename, " NL.jpeg") ||
			strings.HasSuffix(pagename, " NL.png")) {
		return DidNothingError{Pagename: pagename}
	}

	// Get contents of page
	content, timestamp, err := client.GetPageByName(pagename)
	if err != nil {
		return errors.Wrapf(err, "could not get page %v", pagename)
	}
	if strings.Contains(content, "[[Category:New Leaf villager houses]]") {
		return DidNothingError{Pagename: pagename}
	}

	content = strings.Replace(content, "[[Category:New Leaf images]]", "[[Category:New Leaf villager houses]]", 1) // replace only once
	editParams := make(params.Values)
	editParams["title"] = pagename
	editParams["text"] = content
	editParams["summary"] = "Recategorized to villager images. Edited via " + client.UserAgent
	editParams["basetimestamp"] = timestamp
	editParams["bot"] = "1"
	if err = client.Edit(editParams); err != nil {
		return errors.Wrapf(err, "could not edit page %v", pagename)
	}
	return nil
}

// PrintPageAndContents writes to standard output the pagename and its contents.
// It takes only one optional parameter: an int, that determines how many lines of contents it will print out.
// If optional is length zero, if the value is not an int,
// or if there are fewer lines than the provided parameter it will panic.
//
// This is written as a test to see if the Perform function manages to catch the panic.
var PrintPageAndContents PageFunction = func(pagename string, client *mwclient.Client, optional ...interface{}) error {
	lineCount := optional[0].(int)
	contentRaw, _, err := client.GetPageByName(pagename)
	if err != nil {
		return err
	}
	fmt.Println("Page name:", pagename)
	content := strings.Split(contentRaw, "\n")
	for ii := 0; ii < lineCount; ii++ {
		fmt.Println(content[ii])
	}
	return nil
}

// PrintPageAndContentsSafe is like PrintPageAndContents
// but has additional checks to make sure that this function does not panic.
var PrintPageAndContentsSafe PageFunction = func(pagename string, client *mwclient.Client, optional ...interface{}) error {
	if len(optional) < 1 {
		return errors.New("no optional parameters")
	}
	lineCount, ok := optional[0].(int)
	if !ok {
		return errors.Errorf("%v not an int", optional[0])
	}
	fmt.Println("Page name:", pagename)
	contentRaw, _, err := client.GetPageByName(pagename)
	if err != nil {
		return err
	}
	content := strings.Split(contentRaw, "\n")
	for ii := 0; ii < lineCount && ii < len(content); ii++ {
		fmt.Println(content[ii])
	}
	return nil
}

// Perform performs a PageFunction in a single pagename and returns an error.
// If PageFunction panics it will return an appropriate error.
func Perform(pagename string, function PageFunction, client *mwclient.Client, optional ...interface{}) (err error) {
	recoverFunc := func() {
		if r := recover(); r != nil {
			err = errors.Errorf("panic on page %v with value %#v and optional params %#v", pagename, r, optional)
		}
	}
	defer recoverFunc()
	err = function(pagename, client, optional...)
	return err
}

// PerformInPages performs a PageFunction in all pages in a slice of strings
// and returns a slice of errors it encounters.
//
// If PageFunction returns a DidNothingError it will not be counted as an error
// and won't be appended to the error slice.
//
// Errors and glog
//
// This function uses the Google logging module https://github.com/golang/glog for fine tuned logging.
// As the function iterates through the pages it may encounter some problems.
//
// It prints to Info if function returns a DidNothingError to a particular page,
// or if the function performed successfully.
//
// It prints to Error if it couldn't process a particular page using the Perform function.
func PerformInPages(pages []string, function PageFunction, client *mwclient.Client, optional ...interface{}) []error {
	allErrors := make([]error, 0)
	for _, page := range pages {
		err := Perform(page, function, client, optional...)
		if serr, ok := err.(DidNothingError); ok {
			glog.Infof("Did nothing to %v", serr.Pagename)
		} else if err != nil {
			err = errors.Wrapf(err, "could not run function through %v", page)
			allErrors = append(allErrors, err)
			glog.Errorf("Cannot perform function in %v: %v", page, err)
			glog.Errorf("Stack trace: \n%+v", err)
		} else {
			glog.Infof("Successfully performed function on page %v", page)
		}
	}

	return allErrors
}

// PerformInPageChannel performs a PageFunction in all pages on a channel of strings until such channel closes.
// It returns a chan error containing all errors that may occur during the processing,
// which will only be closed once all pages have been processed.
//
// If PageFunction returns a DidNothingError it will not be counted as an error
// and won't be appended to the error slice.
//
// Errors and glog
//
// This function uses the Google logging module https://github.com/golang/glog for fine tuned logging.
// As the function iterates through the pages it may encounter some problems.
//
// It prints to Info if function returns a DidNothingError to a particular page,
// or if the function performed successfully.
//
// It prints to Error if it couldn't process a particular page using the Perform function.
func PerformInPageChannel(pages <-chan string, function PageFunction, client *mwclient.Client, optional ...interface{}) chan error {
	allErrors := make(chan error, 10)
	go func() {
		defer close(allErrors)
		for page := range pages {
			err := Perform(page, function, client, optional...)
			if serr, ok := err.(DidNothingError); ok {
				glog.Infof("Did nothing to %v", serr.Pagename)
			} else if err != nil {
				err = errors.Wrapf(err, "could not run function through %v", page)
				allErrors <- err
				glog.Errorf("Cannot perform function in %v: %v", page, err)
				glog.Errorf("Stack trace: \n%+v", err)
			} else {
				glog.Infof("Successfully performed function on page %v", page)
			}
		}
	}()
	return allErrors
}

// PerformInCategory performs a PageFunction in all pages and subcategories in a category
// and will return a slice of all errors it encounters.
// The category parameter does not need to include the prefix "Category:" or its localized equivalent.
//
// PageFunction
//
// PerformInCategory takes in an arbitrary number of values optional that will be passed
// as optional values in the PageFunction.
// Care must be taken on using the appropriate optional values on a specified PageFunction.
// If the function panics it will continue iterating through the pages in the category,
// and information about the panic will be written to the slice of errors.
//
// Errors and glog
//
// This function uses the Google logging module https://github.com/golang/glog for fine tuned logging.
// As the function iterates through the category pages it may encounter some problems.
//
// It prints to Info if function returns a DidNothingError to a particular page,
// or if the function performed successfully.
//
// It prints to Error if it couldn't process a particular page,
// or if the PageFunction panics as per Perform.
func PerformInCategory(category string, function PageFunction, client *mwclient.Client, optional ...interface{}) []error {
	allErrors := make([]error, 0)
	pages := QueryCategoryMembers(category, client)

	for page := range pages {
		err := Perform(page, function, client, optional...)
		if serr, ok := err.(DidNothingError); ok {
			glog.Infof("Did nothing to %v", serr.Pagename)
		} else if err != nil {
			err = errors.Wrapf(err, "could not run function through %v", page)
			allErrors = append(allErrors, err)
			glog.Errorf("Cannot perform function in %v of category %v: %v", page, category, err)
			glog.Errorf("Stack trace: \n%+v", err)
		} else {
			glog.Infof("Successfully performed function on page %v", page)
		}
	}

	return allErrors
}

// PerformInLinks performs a PageFunction in all pages that title links to
// and will return a slice of all errors it encounters.
// The links of a page does not necessarily include the files in said page.
//
// PageFunction
//
// PerformInLinks takes in an arbitrary number of values optional that will be passed
// as optional values in the PageFunction.
// Care must be taken on using the appropriate optional values on a specified PageFunction.
// If the function panics it will continue iterating through the pages in the category,
// and information about the panic will be written to the slice of errors.
//
// Errors and glog
//
// This function uses the Google logging module https://github.com/golang/glog for fine tuned logging.
// As the function iterates through the links it may encounter some problems.
//
// It prints to Info if function returns a DidNothingError to a particular page,
// or if the function performed successfully.
//
// It prints to Error if it couldn't process a particular page,
// or if the PageFunction panics as per Perform.
//
// Multiple pages on title
//
// If title contains multiple pages (e.g., "Title One|Title Two"),
// function would still work through the links of both pages,
// although this may result in undefined behavior, especially on returning errors.
// Please do not do this.
func PerformInLinks(title string, function PageFunction, client *mwclient.Client, optional ...interface{}) []error {
	allErrors := make([]error, 0)
	links := QueryLinks(title, client)

	for link := range links {
		err := Perform(link, function, client, optional...)
		if serr, ok := err.(DidNothingError); ok {
			glog.Infof("Did nothing to %v", serr.Pagename)
		} else if err != nil {
			err = errors.Wrapf(err, "could not run function through %v", link)
			allErrors = append(allErrors, err)
			glog.Errorf("Cannot perform function in %v linked by %v: %v", link, title, err)
			glog.Errorf("Stack trace: \n%+v", err)
		} else {
			glog.Infof("Successfully performed function on page %v", link)
		}
	}

	return allErrors
}

// PerformInFiles performs a PageFunction in all images and files found in title
// and will return a slice of all errors it encounters.
//
// PageFunction
//
// PerformInFiles takes in an arbitrary number of values optional that will be passed
// as optional values in the PageFunction.
// Care must be taken on using the appropriate optional values on a specified PageFunction.
// If the function panics it will continue iterating through the pages,
// and information about the panic will be written to the slice of errors.
//
// Errors and glog
//
// This function uses the Google logging module https://github.com/golang/glog for fine tuned logging.
// As the function iterates through the links it may encounter some problems.
//
// It prints to Info if function returns a DidNothingError to a particular page,
// or if the function performed successfully.
//
// It prints to Error if it couldn't process a particular page,
// or if the PageFunction panics as per Perform.
//
// Multiple pages on title
//
// If title contains multiple pages (e.g., "Title One|Title Two"),
// function would still work through the links of both pages,
// although this may result in undefined behavior, especially on returning errors.
// Please do not do this.
func PerformInFiles(title string, function PageFunction, client *mwclient.Client, optional ...interface{}) []error {
	allErrors := make([]error, 0)
	files := QueryFiles(title, client)

	for fileTitle := range files {
		err := Perform(fileTitle, function, client, optional...)
		if serr, ok := err.(DidNothingError); ok {
			glog.Infof("Did nothing to %v", serr.Pagename)
		} else if err != nil {
			err = errors.Wrapf(err, "could not run function through %v", fileTitle)
			allErrors = append(allErrors, err)
			glog.Errorf("Cannot perform function in %v used by %v: %v", fileTitle, title, err)
			glog.Errorf("Stack trace: \n%+v", err)
		} else {
			glog.Infof("Successfully performed function on page %v", fileTitle)
		}
	}

	return allErrors
}
