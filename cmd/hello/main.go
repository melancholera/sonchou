// hello is a simple Hello World script,
// written as a demo in running these cmd scripts.
//
// 	./hello
//
// Prints out Hello, Sonchou! and a short message in stdout using fmt.Println.
package main

import "fmt"

func main() {
	fmt.Println("Hello, Sonchou!")
	fmt.Println()
	fmt.Println("...that's all. Nothing to see here really.")
}
