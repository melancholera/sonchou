package main

import (
	"io/ioutil"
	"path/filepath"

	"gitlab.com/melancholera/sonchou/pkg/images"
)

// MaximumBounds returns the record-holders for the largest width and height
// in a given folder
func MaximumBounds(folder string) (widthRecord, heightRecord Record, err error) {
	files, err := ioutil.ReadDir(folder)
	if err != nil {
		return Record{}, Record{}, err
	}
	for _, eachFile := range files {
		if eachFile.IsDir() {
			continue // don't care.
		}
		filename := filepath.Join(folder, eachFile.Name())
		img, err := FilenameToImage(filename)
		if err != nil {
			continue // don't care
		}
		width, height := images.ReadDimensions(img)
		widthRecord.Compare(Record{filename, width})
		heightRecord.Compare(Record{filename, height})
	}
	return
}
