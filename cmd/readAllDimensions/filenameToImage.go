package main

import (
	"image"
	"os"

	"gitlab.com/melancholera/sonchou/pkg/images"
)

// FilenameToImage converts a filename to an image object
func FilenameToImage(filename string) (image.Image, error) {
	rawFile, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	return images.FileToImage(rawFile)
}
