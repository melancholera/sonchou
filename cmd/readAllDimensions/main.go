// readAllDimensions prints out all the dimensions of images in a given directory
// and returns all images that are larger than 104x104px
// as well as the images that have the largest width and height.
//
// Usage
//
// dir is a directory.
//
// 	./readAllDimensions dir
//
// Execution
//
// If dir cannot be opened or if there are no parameters the program will terminate.
// If there are multiple arguments provided it will only use the first one.
//
// Known issues
//
// The 104x104px limit is a constant which it needs not be.
// A flag can be implemented which can change the threshold.
// Some of the functions used in this script can be used for other projects
// but that has not been done.
// Nevertheless, I have to admit that this script and its files are sloppily written
// and need to be reworked in the future.
//
// License
//
// readAllDimensions is part of Project Sonchou,
// which is released under the MIT license.
// See https://gitlab.com/melancholera/sonchou for more information.
package main

import (
	"fmt"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io/ioutil"
	"log"
	"os"
)

var folder string
var files []os.FileInfo
var helpMessage string = `readAllDimensions
Syntax
	./readAllDimensions dir

Reads all images in a directory dir.`

func init() {
	var err error
	if len(os.Args) < 2 {
		log.Fatalln(helpMessage)
	}
	folder = os.Args[1]
	files, err = ioutil.ReadDir(folder)
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	if err := PrintAll(folder); err != nil {
		log.Fatal(err)
	}
	if widthRecord, heightRecord, err := MaximumBounds(folder); err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("largest width: %v (%v)\n", widthRecord.Name, widthRecord.Value)
		fmt.Printf("largest height: %v (%v)\n", heightRecord.Name, heightRecord.Value)
	}
	const bound uint = 104
	if excess, err := CountExceeding(folder, bound, bound); err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("number of images exceeding %vx%v: %v\n", bound, bound, excess)
	}
}
