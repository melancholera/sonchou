package main

// helper functions galore

// Record is a "record"
type Record struct {
	Name  string // current record holder
	Value uint   // the record that person has
}

// Compare compares, and if necessary, replaces the current record holder
func (holder *Record) Compare(contender Record) {
	// holder will be replaced by contender in case if greater
	if contender.Value > holder.Value {
		holder.Name, holder.Value = contender.Name, contender.Value
	}
}
