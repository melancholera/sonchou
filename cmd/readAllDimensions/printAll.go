package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"

	"gitlab.com/melancholera/sonchou/pkg/images"
)

// PrintAll prints all dimensions of pictures in a certain folder to stdout
func PrintAll(folder string) error {
	files, err := ioutil.ReadDir(folder)
	if err != nil {
		return err
	}
	for _, eachFile := range files {
		if eachFile.IsDir() {
			continue // don't care.
		}
		filename := filepath.Join(folder, eachFile.Name())
		img, err := FilenameToImage(filename)
		if err != nil {
			continue // don't care
		}
		width, height := images.ReadDimensions(img)
		fmt.Printf("%v: %vx%vpx\n", filename, width, height)
	}
	return nil
}
