package main

import (
	"io/ioutil"
	"path/filepath"

	"gitlab.com/melancholera/sonchou/pkg/images"
)

// CountExceeding counts how many images
// in a given folder will exceed width w or height h.
func CountExceeding(folder string, w, h uint) (count int, err error) {
	files, err = ioutil.ReadDir(folder)
	if err != nil {
		return 0, err
	}
	for _, eachFile := range files {
		if eachFile.IsDir() {
			continue // don't care.
		}
		filename := filepath.Join(folder, eachFile.Name())
		img, err := FilenameToImage(filename)
		if err != nil {
			continue // don't care
		}
		width, height := images.ReadDimensions(img)
		if width > w || height > h {
			count++
		}
	}
	return count, nil

}
