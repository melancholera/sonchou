package main

import (
	"bufio"
	"os"
	"strings"

	"github.com/golang/glog"
)

// OpenLookup will open a lookup file as described in the README
// and will return a map representing the lookup file and an error
func OpenLookup(filename string) (map[string]string, error) {
	forbiddenCharacters := `<>:"/\|?%*`
	forbiddenFilenames := []string{".", "..", ""}
	lookup := make(map[string]string)
	lookupFile, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	sc := bufio.NewScanner(lookupFile)
	lineCount := 0

FORALL:
	for sc.Scan() {
		lineCount++
		buffer := sc.Text()

		// trim all text after `//`
		commentsTrimmed := strings.Split(buffer, "//")
		if len(commentsTrimmed) == 0 || len(commentsTrimmed[0]) == 0 {
			glog.Warningf("Ignoring ln %v with contents %q: comment line\n",
				lineCount, buffer)
			continue FORALL
		}
		buffer = commentsTrimmed[0]

		// trim the files with a colon
		filenames := strings.Split(buffer, ":")
		if len(filenames) != 2 {
			glog.Warningf("Ignoring ln %v with contents %q for some reason\n",
				lineCount, buffer)
			continue FORALL
		}

		for i := range filenames {
			filenames[i] = strings.TrimSpace(filenames[i])
			for _, v := range forbiddenFilenames {
				if filenames[i] == v {
					glog.Warningf("Ignoring ln %v with filename %q: forbidden filename\n",
						lineCount, filenames[i])
					continue FORALL
				}
			}
			if strings.ContainsAny(filenames[i], forbiddenCharacters) {
				glog.Warningf("Ignoring ln %v with filename %q: contains forbidden character\n",
					lineCount, filenames[i])
				continue FORALL
			}
		}
		// okay looks like filenames[0]:filenames[1] are okay naems
		if i, ok := lookup[filenames[0]]; ok {
			glog.Warningf("Overriding %q containing %q with %q\n",
				filenames[0], i, filenames[1])
		}
		lookup[filenames[0]] = filenames[1]
	}
	return lookup, lookupFile.Close()
}
