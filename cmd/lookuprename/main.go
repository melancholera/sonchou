// lookuprename renames all files in specified directories
// using a lookup file as a basis.
//
// Usage
//
// Files in directory and in moreDirectories will be renamed in-place.
//
//	./lookuprename lookupFile directory [ moreDirectories ... ]
//
// Lookup file format
//
// Files with filename oldName will be renamed to newName. The two are separated with a colon.
//
//	// All comments start with two slashes.
//	// Blank lines like below will be ignored
//
//	oldName : newName  // another comment
//
// oldName and newName must be valid names.
// Valid names are any strings that do not contain the followwing forbidden characters:
//
//	<>:"/\|?%*
//
// and are not any of the following:
//
//	.
//	..
//	{just whitespace.}
//
// Errors and glog
//
// The program will terminate execution if there are less than two arguments provided
// (i.e., either zero or one arguments are passed),
// or if the lookup file could not be opened.
//
// This script uses the Google logging module
// https://github.com/golang/glog
// for fine tuned logging.
// As the script iterates through the directories
// it may encounter files that it could not open:
//
// It prints to Error if it could not open a directory
// or if it could not rename a file properly.
//
// It prints to Warning
// if the entire line is apparently a comment,
// if there are no colons in a line,
// if a line contains a forbidden character,
// if one of the filenames provided is invalid,
// or if one of the lines would redefine a key-value pair in the map.
// If any of the mentioned conditions occurs except for the very last one,
// it will skip parsing that line.
//
// Issues
//
// The set of old and new names in the lookup file must be such that
// all names must be unique,
// that is no pair between the two sets have the same value,
// and no two old names or new names have the same value.
//
// Violating the above principle may result in undefined behavior.
// If the line would redefine a key-value pair in the map containing all the lines
// that have been parsed, it will happily do so.
// If a file could not be renamed because it already exists,
// it may actually replace the new file.
//
// Please maintain the integrity of your lookup file.
// This script will not correct any problems in your lookup file
// and is hastily written to just perform renames.
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/golang/glog"
)

const helpMsg = `lookuprename

Usage:
	%v lookupFile directory [ moreDirectories ... ]
`

// allFiles[0] is lookupFile; allFiles[1:] are directories
var allFiles []string

func usage() {
	fmt.Fprintf(os.Stderr, helpMsg, os.Args[0])
	flag.PrintDefaults()
}

func init() {
	// do things before everything else
	flag.Usage = usage
	flag.Parse()
	allFiles = flag.Args()
}

func main() {
	var err error
	if len(allFiles) < 2 {
		fmt.Println(allFiles)
		flag.Usage()
		os.Exit(2)
	}
	lookup, err := OpenLookup(allFiles[0])
	if err != nil {
		glog.Fatalf("Error reading %v: %v", allFiles[1], err)
	}
	// now read the lookup again..?
	for key, val := range lookup {
		fmt.Printf("%q -> %q\n", key, val)
	}
	for _, eachDirectory := range allFiles[1:] {
		directoryFiles, err := ioutil.ReadDir(eachDirectory)
		if err != nil {
			glog.Errorf("Error reading directory %v: %v", eachDirectory, err)
			continue
		}
		for _, eachFile := range directoryFiles {
			// check whether file even exists
			newname, found := lookup[eachFile.Name()]
			if !found {
				continue
			}
			oldFilename := filepath.Join(eachDirectory, eachFile.Name())
			newFilename := filepath.Join(eachDirectory, newname)
			if err := os.Rename(oldFilename, newFilename); err != nil {
				glog.Errorf("Error in %q -> %q: %v", oldFilename, newFilename, err)
			}
		}
	}
}
