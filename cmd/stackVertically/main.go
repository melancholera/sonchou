// stackVertically arranges images vertically
// with no gap in between and centrered horizontally.
// This uses images.StackVertically.
//
// Usage
//
// If destination exists, it will be overridden.
// If destination ends with `.jpg` it will be saved as a JPEG image.
// Otherwise it will always be saved as a PNG image.
//
//	./stackVertically destination source [source1 [source2 ... ]]
//
// The program should be able to support GIF, BMP, JPG, and PNG images.
// For GIF images, only the first still will be used.
//
// Errors
//
// If any of the sources could not be opened, for any reason,
// or if destination could not be written to,
// the program will terminate.
package main

import (
	"fmt"
	"image"
	"os"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/melancholera/sonchou/pkg/images"
)

const helpMessage = `stackVertically

Syntax:
	%v destination source [source1 [source2 ... ]]

Stacks all source images and saves to destination.
`

func errorAndExit(err error) {
	fmt.Printf(helpMessage, os.Args[0])
	fmt.Printf("error: %T %v\n", err, err)
	fmt.Printf("stack trace:\n%+v\n", err)
	os.Exit(1)
}

func init() {
	// check if os.Args is at least length 3
	// i.e., destination and at least one source specified.
	if len(os.Args) <= 1 {
		errorAndExit(errors.New("missing destination"))
	}
	if len(os.Args) == 2 {
		errorAndExit(errors.New("missing at least one source"))
	}
}

func main() {
	var err error

	// construct a slice of images
	allSources := make([]image.Image, 0, len(os.Args)-2)
	for ii := 2; ii < len(os.Args); ii++ {
		file, err := os.Open(os.Args[ii])
		if err != nil {
			errorAndExit(errors.Wrapf(err, "could not open file %v", os.Args[ii]))
		}

		img, err := images.FileToImage(file)
		if err != nil {
			errorAndExit(errors.Wrapf(err, "could not open file %v as image", os.Args[ii]))
		}

		allSources = append(allSources, img)
	}

	// Now stack
	result := images.StackVertically(allSources[0], allSources[1:]...)
	if strings.HasSuffix(os.Args[1], ".jpg") {
		err = images.SaveToJPG(result, os.Args[1])
	} else {
		err = images.SaveToPNG(result, os.Args[1])
	}
	if err != nil {
		errorAndExit(errors.Wrapf(err, "could not save to %v", os.Args[1]))
	}

	// Program ended yay
}
