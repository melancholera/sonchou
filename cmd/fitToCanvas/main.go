// fitToCanvas allows the user to modify photos so that they could have some padding around them.
// This is done either by making sure a photo is inside some bounded area
// or by simply adding padding to the image.
// This command manipulates all the images in a particular source folder
// and writes all the files in destination.
// It will ignore all subfolders in source.
//
// Normal mode
//
// Normal mode makes sure each picture is in a grid BOUND pixels wide by BOUNDY pixels high,
// in the center of a CANVAS pixels wide by CANVASY pixels high PNG image.
// and will resize the picture if necessary just to be in said bounds.
// If BOUNDY or CANVASY are not provided, they will take their values from BOUND or CANVAS,
// thus resulting in a square bound grid or a square canvas image.
//
// The images will be in PNG format with a transparent background.
// If the image to be opened contains a solid background,
// it will keep the solid background but will still add the transparent padding.
//
// BOUND, BOUNDY, CANVAS, and CANVASY are uint and must be at least 50.
// The limit is chosen as images with a smaller bound or canvas size would be ridiculously small.
// Choosing the bound grid to be larger than the canvas grid may lead to unexpected behavior.
//
// 	./fitToCanvas -b BOUND [-by BOUNDY] -c CANVAS [-cy CANVASY] source destination
//
//
// Inferred mode
//
// Inferred mode creates a transparent padding of at least PAD pixels on all sides of an image.
// The script will invoke Inferred mode if -infer is passed and PAD is not zero.
// The written images will have equal width and height,
// unless if -nosquare is passed, which simply adds PAD pixels to all sides.
//
// If -atleast is passed, it will make sure that the written image
// is at least MINSIZE pixels wide by MINSIZE pixels high,
// and will write more padding if necessary.
//
// 	./fitToCanvas -infer PAD [-nosquare] [-atleast MINSIZE] source destination
//
// Errors and glog
//
// The program will terminate execution if it encounters an error,
// which occurs for several reasons.
// Some ways to return an error are if the non-flag arguments are too few
// (missing a source or destination folder?),
// too many (don't provide multiple folders),
// if source is not a directory,
// or if destination cannot be created or is not a directory.
// Nevertheless, if the program cannot continue,
// it will produce the stack trace to stdout.
//
// This script uses the Google logging module
// https://github.com/golang/glog
// for fine tuned logging.
// As the script iterates through the source folder,
// it may encounter files that it could not open:
//
// It prints to Error if it could not write the file to the destination folder.
//
// It prints to Warning if a "file" is actually a directory,
// if a file cannot be opened, or if a file cannot be parsed as an image.
//
// It prints to Info the stack traces of what it prints to Warning and Error,
// as well as if an image has been saved.
//
// Note that printing these Errors and Warnings will not result to halting execution.
// The script will continue reading through the files nevertheless.
//
// Examples
//
// All images should be in a 48x48px bound grid in a 64x64px image:
//
//	./fitToCanvas -b 48 -c 64 source destination
//
// All images should have at least 10px of padding and make sure images are at least 64x64px:
//
//	./fitToCanvas -infer 10 -atleast 64 source destination
//
// Do the previous command but overwrite the files:
//
//	./fitToCanvas -infer 10 -atleast 64 source source
//
// License
//
// fitToCanvas is part of Project Sonchou,
// which is released under the MIT license.
// See https://gitlab.com/melancholera/sonchou for more information.
package main

import (
	"flag"
	"fmt"
	"image"
	"os"
	"path/filepath"

	"github.com/golang/glog"
	"github.com/pkg/errors"
	"gitlab.com/melancholera/sonchou/pkg/files"
	"gitlab.com/melancholera/sonchou/pkg/images"
)

// normal mode
var boundWidth, boundHeight uint   // size of the image
var canvasWidth, canvasHeight uint // size of the canvas

// inferred mode
var inferredPadding uint // size of padding for inferred mode
var nosquare bool        // do we want to keep rectangle?
var minsize uint         // minimum size of the photo (inferred mode)

var source, destination string // source and destination folders

const helpMessage string = `fitToCanvas
Syntax:
	%v [ -infer PAD ... | -b BOUND -c CANVAS ... ] source destination

"Transforms" images to BOUNDxBOUNDY inside CANVASxCANVASY
or infer creates a padding of at least PAD pixels around the image.

See README.adoc for more information.
` // %v for program name; ... for other infer flags that people may care about

// maxUint returns the maximum of two uint's
func maxUint(a, b uint) uint {
	if a > b {
		return a
	}
	return b
}
func errorAndExit(err error) {
	fmt.Printf(helpMessage, os.Args[0])
	fmt.Printf("error: %T %v\n", err, err)
	fmt.Printf("stack trace:\n%+v\n", err)
	os.Exit(1)
}

func init() {
	flag.UintVar(&boundWidth, "b", 0, "width of the image")
	flag.UintVar(&canvasWidth, "c", 0, "width of the canvas")
	flag.UintVar(&boundHeight, "by", 0, "(optional) height of the image")
	flag.UintVar(&canvasHeight, "cy", 0, "(optional) height of the canvas")
	flag.UintVar(&inferredPadding, "infer", 0, "padding to add for inferred mode")
	flag.UintVar(&minsize, "atleast", 0, "minimum size of the image for inferred mode")
	flag.BoolVar(&nosquare, "nosquare", false, "do not keep square canvas")

	flag.Parse()
	if len(flag.Args()) < 2 {
		errorAndExit(errors.New("missing source or destination address"))
	} else if len(flag.Args()) > 2 {
		errorAndExit(errors.New("too many arguments"))
	}
	source = flag.Args()[0]
	destination = flag.Args()[1]

	// now check if images are "too" small
	if inferredPadding == 0 {
		if boundHeight == 0 {
			boundHeight = boundWidth
		}
		if canvasHeight == 0 {
			canvasHeight = canvasWidth
		}
		if boundWidth < 50 {
			errorAndExit(fmt.Errorf("bound (%v) below 50", boundWidth))
		}
		if boundHeight < 50 {
			errorAndExit(fmt.Errorf("boundy (%v) below 50", boundHeight))
		}
		if canvasWidth < 50 {
			errorAndExit(fmt.Errorf("canvas (%v) below 50", canvasWidth))
		}
		if canvasHeight < 50 {
			errorAndExit(fmt.Errorf("canvasy (%v) below 50", canvasHeight))
		}
	}
	// once we're done with initial checks let's go to the program proper
}

func main() {
	var err error
	// create directory for Destination
	if err = files.CreateDirectory(destination); err != nil {
		errorAndExit(errors.WithMessagef(err, "could not create destination directory %v", destination))
	}

	// let's try to read the files in Source
	allFiles, err := files.FolderToFiles(source)
	if err != nil {
		errorAndExit(errors.WithMessagef(err, "could not open source directory %v", source))
	}

	for _, datum := range allFiles {
		sourceFilename := filepath.Join(source, datum.Name())
		destinationFilename := filepath.Join(destination, datum.Name())
		if datum.IsDir() {
			glog.Warningf("file %v is directory, ignoring...", sourceFilename)
			continue // don't care.
		}

		file, err := os.Open(sourceFilename)
		if err != nil {
			// it can't open the file properly
			glog.Warningf("file %v cannot be opened, ignoring...", sourceFilename)
			glog.Infof("stack trace: \n%+v\n", err)
			continue
		}

		img, err := images.FileToImage(file)
		if err != nil {
			// it can't open the file as an image
			// Warning used because it's not often that a file is not an image
			glog.Warningf("file %v cannot be parsed as an image, ignoring...", sourceFilename)
			glog.Infof("stack trace: \n%+v\n", err)
			continue
		}

		if inferredPadding != 0 {
			// then pull out some math
			boundWidth, boundHeight = images.ReadDimensions(img)
			// note 2*inferredPadding as we want the padding to be
			// that amount for all sides.
			// without it, padding would be halved.
			canvasWidth, canvasHeight = boundWidth+2*inferredPadding, boundHeight+2*inferredPadding
			if !nosquare {
				canvasWidth, canvasHeight = maxUint(canvasWidth, canvasHeight), maxUint(canvasWidth, canvasHeight)
			}
			if minsize != 0 {
				canvasWidth, canvasHeight = maxUint(canvasWidth, minsize), maxUint(canvasHeight, minsize)
			}
		}
		newImg := images.FitToDimensions(img, image.Pt(int(boundWidth), int(boundHeight)), image.Pt(int(canvasWidth), int(canvasHeight)))

		// now save newImg to destination
		if err = images.SaveToPNG(newImg, destinationFilename); err != nil {
			glog.Errorf("could not save to PNG file %v", destinationFilename)
			glog.Infof("stack trace: \n%+v\n", err)
		} else {
			glog.Infof("Saved: %v -> %v", sourceFilename, destinationFilename)
		}
	}
}
