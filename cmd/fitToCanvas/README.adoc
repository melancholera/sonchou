= fitToCanvas

....
./fitToCanvas [ -infer PAD [-nosquare] [-atleast MINSIZE] | -b BOUND [-by BOUNDY] -c CANVAS [-cy CANVASY]] source destination
....

== Normal mode

`fitToCanvas` performs `images/FitToDimensions` to all images in a folder `source`
where all images will be resized to fit inside a `BOUND` by `BOUNDY` image
and is placed in a `CANVAS` by `CANVASY` image.
If `BOUNDY` and `CANVASY` are not provided,
they will default to `BOUND` and `CANVAS` respectively.

Since otherwise images would be _too_ small,
`BOUND`, `BOUNDY`, `CANVAS`,and `CANVASY`
must be at least 50.

All images will be "converted" and saved into a folder `destination`
with the same filename.

== Inferred mode

Inferred mode is enabled if `-infer` is passed.

Considering that the images parsed may be of different sizes,
inferred mode creates a padding of at least PAD pixels on all sides
but keeps the canvas as a square,
unless if `-nosquare` is passed.

If `-atleast` is passed,
and the photo's size is still less than `MINSIZExMINSIZE`
after being added with padding,
then the canvas becomes `MINSIZExMINSIZE`.

