module gitlab.com/melancholera/sonchou

go 1.13

require (
	github.com/antonholmquist/jason v1.0.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
	gitlab.com/melancholera/go-mwclient v1.1.2
	golang.org/x/image v0.0.0-20191214001246-9130b4cfad52
)
