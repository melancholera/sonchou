// Package sonchou is not really a package,
// but rather a collection of packages and scripts in its subdirectories.
// Most of these are for internal use in Nookipedia (https://nookipedia.com),
// but feel free to use these packages in your projects,
// so long as you follow the license as follows
//
// Compiling scripts
//
// All the scripts for Project Sonchou are in the cmd folder.
// This is chosen as standard in Go projects.
// To compile a command, for instance, hello,
// one could run the following command in the program root:
//
//	go build -o hello.out ./cmd/hello
//
// The leading dot-slash is necessary to signal
// that the command is relative the folder and not in $GOPATH,
// a concept in Go where all Go projects are supposed to be stored.
// The .out extension is chosen since this repository's .gitignore
// is written to ignore files that end with .out.
// For Windows users, however, the usage of .exe is recommended:
//
// 	go build -o hello.exe .\cmd\hello
//
// Running the file can be as simple as
//
// 	./hello.out
//
// or
//
//	.\hello.exe
//
// Do note that some scripts will require the passing of flags,
// which are parameters after the name of the file.
// Refer to the scripts' documentation for more information.
//
// License
//
// MIT License
//
//	Copyright (c) 2019 Vibrio Melancholia
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//	SOFTWARE.
//
// Third-party software notices
//
// See NOTICE.md in this repository.
package sonchou
